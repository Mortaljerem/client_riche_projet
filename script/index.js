$(document).ready(function(){

var apiKey = "2e90ca5a45413f7ae6147f2bda866d7f";
var idVille= null;
var pageActuelle = 1;
var minTak;
var tmp;
var nbrPage;
var b=true;
var tri = "date-taken-desc";


$('#commune').focus();
//$('#imgWrapper').css('display','none');
$('#carousel').css('display','none');
$('footer').css('display','none');


//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
//--------------------------- RECUPERATION DES PHOTOS DE LA PAGE----------------------------------------------
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------

$("#submit").on("click", function(){

	$('#trinom').prop('disabled',false);
	$('#trinom').css('opacity','1');	
	$('#triphotographe').prop('disabled',false);
	$('#triphotographe').css('opacity','1');
	$('#tridate').prop('disabled',false);
	$('#tridate').css('opacity','1');
	
	if($('#datepicker').val() != ""){
	var reg = new RegExp(/^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/);
	var b2 = reg.test($('#datepicker').val());

		if(b2){}
		else{b=false;}
	}

	if(($("#commune").val() != "" ) && b){

		var requete = "https://api.flickr.com/services/rest/?method=flickr.places.find&api_key="+apiKey
			+"&query="+ $("#commune").val()
			+"&format=json&jsoncallback=?";


		$.getJSON(requete,function(data){


			/* ------------------------------------------------------------------------------------------------
			Gestion des demandes utilisateur :
			--------------------------------------------------------------------------------------------------*/
			// Gérer les donées de requetes : place, page, nomre par page , tri, date min de prise de vue(a initialiser le jour même(datepicker))

			// La ville :
			idVille = data.places.place[0].place_id;

			// La page souhaité :
			pageActuelle = 1;
			$('#page').html(pageActuelle);

			// la date de prise de vue min :
			tmp=$('#datepicker').val();
			var t = new Array();
			t = tmp.split("/");
			minTak=(t[1]+"/"+t[0]+"/"+t[2]);

			// Nomre de phto par page :
			nbrPage = parseInt($('#photos_number option:selected').val());
			
			// Création de la requete adéquate
			if($('#datepicker').val()==""){
			var requete2 = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key="+apiKey
				+"&place_id="+idVille
				+"&per_page="+nbrPage
				+"&page="+pageActuelle
				+"&sort="+tri
				+"&format=json&jsoncallback=?";
			}
			else if($('#datepicker').val()!=""){
				var requete2 = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key="+apiKey
				+"&place_id="+idVille
				+"&per_page="+nbrPage
				+"&page="+pageActuelle
				+"&sort="+tri
				+"&min_taken_date="+minTak
				+"&format=json&jsoncallback=?";
			}


			$('#imgWrapper').html('');
			$('ul#carousel2').html('');

			$.getJSON(requete2, function(data){
				if((data.photos.photo.length > 0)  || (data.photos.photo.length > 1)){
					console.log((data.photos.photo.length));
					//console.log(data.photos.photo.length);
					$.each(data.photos.photo, function(i,item){
						//console.log(item.farm)
						//console.log(item.title);
						var url = "https://farm"+item.farm+".staticflickr.com/"+item.server+"/"+item.id+"_"+item.secret+".jpg";
						$.ajax({
							type: "GET",
							dataType:"json",
							url: "https://www.flickr.com/services/rest/?method=flickr.photos.getInfo",
							data : 'api_key=' + apiKey + "&photo_id="+item.id+"&secret="+item.secret+"&format=json&nojsoncallback=1",
							success: function(data){
								console.log(data);
								if(!$('#check').is(':checked')){
									$('#carousel').css('display','none');
									$('#imgWrapper').css('display','block');
									$('footer').css('display','inline-block');
									
									var dateU = new Date(parseInt(data.photo.dateuploaded) * 1000);
									var lasteU = new Date(parseInt(data.photo.dates.lastupdate) *1000);

									$("<img>").attr('src',url).
										attr('identifiant',data.photo.id).
										attr('secret',data.photo.secret).
										attr('title',data.photo.title._content).
										attr('dateU',dateU).
										attr('lastU',lasteU).
										attr('taken',data.photo.dates.taken).
										attr('pays',data.photo.location.country._content).
										attr('ville',data.photo.location.locality._content).
										attr('region',data.photo.location.region._content).
										attr('proprio',data.photo.owner.realname).
										attr('class','image').
										appendTo('#imgWrapper');
								}
								else{
									$('#imgWrapper').css('display','none');
									$('#carousel').css('display','inline-block');
									$('footer').css('display','none');

									var dateU = new Date(parseInt(data.photo.dateuploaded) * 1000);
									var lasteU = new Date(parseInt(data.photo.dates.lastupdate) *1000);
									
									$("<img>").attr('src',url).
										attr('identifiant',data.photo.id).
										attr('secret',data.photo.secret).
										attr('title',data.photo.title._content).
										attr('dateU',dateU).
										attr('lastU',lasteU).
										attr('taken',data.photo.dates.taken).
										attr('pays',data.photo.location.country._content).
										attr('ville',data.photo.location.locality._content).
										attr('region',data.photo.location.region._content).
										attr('proprio',data.photo.owner.realname).
										attr('width','600').
										attr('height','400').
										appendTo('#carousel2').
										wrap('<li></li>');
								}
							},
							error : function(request, error) { // Info Debuggage si erreur        
				                       alert("Erreur : responseText: "+request.responseText);
				            }
						});
					});
				}
				else{
					alert("Malheuresement aucune photos n'est disponible pour votre recherche");
				}
			});		
		});
	}
	else{
		alert("le champs de recherche de ville est vide ou la date n'est pas au bon format");
	}				
});


//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
//--------------------------- AJOUT DES INFOS LORS DU CLIC D'UNE IMAGE NORMALE--------------------------------
//----------------------------------------------(POPUP)-------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
$('#imgWrapper').delegate('img','click',function(){
	$('#popup').css('display','block');
	
	console.log("display ok");
	
	//console.log($(this));
	//console.log($(this)[0].attributes.identifiant.value);
	var id = $(this)[0].attributes.identifiant.value;
	var titre = $(this)[0].attributes.title.value;
	var prise = $(this)[0].attributes.taken.value;
	var upload = $(this)[0].attributes.dateU.value;
	var modif = $(this)[0].attributes.lastU.value;
	var source = $(this)[0].attributes.src.value
	var pays = $(this)[0].attributes.pays.value;
	var region = $(this)[0].attributes.region.value;
	var ville = $(this)[0].attributes.ville.value;
	var proprio = $(this)[0].attributes.proprio.value;
	
	$('#popup').html("<div id='buttonClose'></div>"
		+"<h2>"+titre+"</h2></br>"
		+"<img src='"+source+"'></img>"
		+"<br/><br/>Date de prise de vue : "+prise
		+"<br/>Date d'upload : "+upload
		+"<br/>Date de dernière modification : "+modif
		+"</br> Pays : "+pays
		+"</br> Region : "+region
		+"</br> Ville : "+ville
		+"</br> Photographe : "+proprio+"</h2>");
});


//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
//--------------------------- AJOUT DES INFOS LORS DU CLIC D'UNE IMAGE CAROUSEL-------------------------------
//----------------------------------------------(POPUP)-------------------------------------------------------
//------------------------------------------------------------------------------------------------------------

$('#carousel2').delegate('img','click',function(){
	$('#popup').css('display','block');
	
	//console.log($(this));
	//console.log($(this)[0].attributes.identifiant.value);
	var id = $(this)[0].attributes.identifiant.value;
	var titre = $(this)[0].attributes.title.value;
	var prise = $(this)[0].attributes.taken.value;
	var upload = $(this)[0].attributes.dateU.value;
	var modif = $(this)[0].attributes.lastU.value;
	var source = $(this)[0].attributes.src.value
	var pays = $(this)[0].attributes.pays.value;
	var region = $(this)[0].attributes.region.value;
	var ville = $(this)[0].attributes.ville.value;
	var proprio = $(this)[0].attributes.proprio.value;
	
	$('#popup').html("<div id='buttonClose'></div>"
		+"<h2>"+titre+"</h2></br>"
		+"<img src='"+source+"'></img>"
		+"<br/><br/>Date de prise de vue : "+prise
		+"<br/>Date d'upload : "+upload
		+"<br/>Date de dernière modification : "+modif
		+"</br> Pays : "+pays
		+"</br> Region : "+region
		+"</br> Ville : "+ville
		+"</br> Photographe : "+proprio+"</h2>");
});


$('#content').delegate('#popup #buttonClose','click',function(){
		$('#popup').css('display','none');
});


//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
//---------------------------------------------- PAGE SUIVANTE------------------------------------------------
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
$("#nextPageButton").on("click", function(){
	//console.log($('#photos_number option:selected').attr("value"));
	pageActuelle = (pageActuelle)+1;
	//console.log(pageActuelle);
	$('#page').html(pageActuelle);
	var requete2 = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key="+apiKey
		+"&place_id="+idVille
		+"&per_page="+$('#photos_number option:selected').text()
		+"&page="+pageActuelle
		+"&format=json&jsoncallback=?";

	$('#imgWrapper').html('');

	$.getJSON(requete2, function(data){
		if(data.photos.photo.length!=0){
			//console.log(data.photos.photo.length);
			//console.log(data);

			$.each(data.photos.photo, function(i,item){
				//console.log(item.farm)
				//console.log(item.title);
				var url = "https://farm"+item.farm+".staticflickr.com/"+item.server+"/"+item.id+"_"+item.secret+".jpg";
				$.ajax({
					type: "GET",
					dataType:"json",
					url: "https://www.flickr.com/services/rest/?method=flickr.photos.getInfo",
					data : 'api_key=' + apiKey + "&photo_id="+item.id+"&secret="+item.secret+"&format=json&nojsoncallback=1",
					success: function(data){
						//console.log(data.photo);
						$("<img>").attr('src',url).
							attr('identifiant',data.photo.id).
							attr('secret',data.photo.secret).
							attr('title',data.photo.title._content).
							attr('dateU',data.photo.dateuploaded).
							attr('lastU',data.photo.dates.lastupdate).
							attr('taken',data.photo.dates.taken).
							attr('pays',data.photo.location.country._content).
							attr('ville',data.photo.location.locality._content).
							attr('region',data.photo.location.region._content).
							attr('proprio',data.photo.owner.realname).
							attr('class','image').
							appendTo('#imgWrapper');
					},
					error : function(request, error) { // Info Debuggage si erreur        
		                       alert("Erreur : responseText: "+request.responseText);
		            }
				});
				
			});
		}
		else{
			alert("Malheuresement aucune photos n'est disponible pour votre recherche");
		}
	});
});



//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
//---------------------------------------------- PAGE PECEDENTE-----------------------------------------------
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
$("#previousPageButton").on("click", function(){
	//console.log($('#photos_number option:selected').attr("value"));
	if(pageActuelle > 1){
		pageActuelle = (pageActuelle)-1;
	}	
	//console.log(pageActuelle);
	$('#page').html(pageActuelle);
	var requete2 = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key="+apiKey
		+"&place_id="+idVille
		+"&per_page="+$('#photos_number option:selected').text()
		+"&page="+pageActuelle
		+"&format=json&jsoncallback=?";

	$('#imgWrapper').html('');

	$.getJSON(requete2, function(data){
		if(data.photos.photo.length!=0){
			//console.log(data.photos.photo.length);
			//console.log(data);

			$.each(data.photos.photo, function(i,item){
				//console.log(item.farm)
				//console.log(item.title);
				var url = "https://farm"+item.farm+".staticflickr.com/"+item.server+"/"+item.id+"_"+item.secret+".jpg";
				$.ajax({
					type: "GET",
					dataType:"json",
					url: "https://www.flickr.com/services/rest/?method=flickr.photos.getInfo",
					data : 'api_key=' + apiKey + "&photo_id="+item.id+"&secret="+item.secret+"&format=json&nojsoncallback=1",
					success: function(data){
						//console.log(data.photo);
						$("<img>").attr('src',url).
							attr('identifiant',data.photo.id).
							attr('secret',data.photo.secret).
							attr('title',data.photo.title._content).
							attr('dateU',data.photo.dateuploaded).
							attr('lastU',data.photo.dates.lastupdate).
							attr('taken',data.photo.dates.taken).
							attr('pays',data.photo.location.country._content).
							attr('ville',data.photo.location.locality._content).
							attr('region',data.photo.location.region._content).
							attr('proprio',data.photo.owner.realname).
							attr('class','image').
							appendTo('#imgWrapper');
					},
					error : function(request, error) { // Info Debuggage si erreur        
		                       alert("Erreur : responseText: "+request.responseText);
		            }
				});
				
			});
		}
		else{
			alert("Malheuresement aucune photos n'est disponible pour votre recherche");
		}
	});
});



//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
//---------------------------------------------- Tri ---------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------

$('#trinom').on('click',function(){

	console.log($('#imgWrapper img').length);
	if($('#imgWrapper img').length > 1 ) {
		$(this).prop('disabled',true);
		$(this).css('opacity','0.5');
		$('#tridate').prop('disabled',false);
		$('#tridate').css('opacity','1');
		$('#triphotographe').prop('disabled',false);
		$('#triphotographe').css('opacity','1');

		$wrap = $("#imgWrapper") ;
		// Capture des lignes du tableau
		$wrap.children("img").sort(function(ligneA,ligneB) {
		// fonction de tri selon l’identifiant
		var valeurA = $(ligneA).attr("title") ;
		var valeurB = $(ligneB).attr("title") ;
		return valeurA > valeurB ? 1 : -1 ;
		})
		// Les lignes sont ajoutées à $wrap dans l’ordre de tri
		.appendTo($wrap);

	}
	else{
		alert("Il n'y a pas assez de photo pour trier !");
	}

});

$('#tridate').on('click',function(){

	console.log($('#imgWrapper img').length);
	if($('#imgWrapper img').length > 1 ) {
		$(this).prop('disabled',true);
		$(this).css('opacity','0.5');
		$('#trinom').prop('disabled',false);
		$('#trinom').css('opacity','1');
		$('#triphotographe').prop('disabled',false);
		$('#triphotographe').css('opacity','1');

		$wrap = $("#imgWrapper") ;
		// Capture des lignes du tableau
		$wrap.children("img").sort(function(ligneA,ligneB) {
		// fonction de tri selon l’identifiant
		var valeurA = $(ligneA).attr("taken") ;
		var valeurB = $(ligneB).attr("taken") ;
		return valeurA > valeurB ? 1 : -1 ;
		})
		// Les lignes sont ajoutées à $wrap dans l’ordre de tri
		.appendTo($wrap);
	}
	else{
		alert("Il n'y a pas assez de photo pour trier !");
	}

});

$('#triphotographe').on('click',function(){

	console.log($('#imgWrapper img').length);
	if($('#imgWrapper img').length > 1 ) {
		$(this).prop('disabled',true);
		$(this).css('opacity','0.5');
		$('#tridate').prop('disabled',false);
		$('#tridate').css('opacity','1');
		$('#trinom').prop('disabled',false);
		$('#trinom').css('opacity','1');

		$wrap = $("#imgWrapper") ;
		// Capture des lignes du tableau
		$wrap.children("img").sort(function(ligneA,ligneB) {
		// fonction de tri selon l’identifiant
		var valeurA = $(ligneA).attr("proprio") ;
		var valeurB = $(ligneB).attr("proprio") ;
		return valeurA > valeurB ? 1 : -1 ;
		})
		// Les lignes sont ajoutées à $wrap dans l’ordre de tri
		.appendTo($wrap);
	}
	else{
		alert("Il n'y a pas assez de photo pour trier !");
	}

});

});

